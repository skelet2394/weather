//
//  String+Extensions.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation


public protocol StringConvertible {
    func asString() -> String
}


struct StringConvertibeWrapper<Wrapped>: StringConvertible {
    let wrapped: Wrapped
    let convert: (Wrapped) -> String
    
    func asString() -> String {
        return convert(wrapped)
    }
}


extension StringConvertibeWrapper: Equatable where Wrapped: Equatable {
    static func == (lhs: StringConvertibeWrapper<Wrapped>, rhs: StringConvertibeWrapper<Wrapped>) -> Bool {
        return lhs.wrapped == rhs.wrapped
    }
}


extension StringConvertibeWrapper: Hashable where Wrapped: Hashable {
    func hash(into hasher: inout Hasher) {
        wrapped.hash(into: &hasher)
    }
}


extension String: StringConvertible {
    public func asString() -> String {
        return self
    }
}

//
//  JSONDecoder.swift
//  Weather
//
//  Created by Valery Silin on 23/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation


public final class DefaultJSONDecoder: JSONDecoder {
    override public init() {
        super.init()
        if #available(iOS 10.0, *) {
            self.dateDecodingStrategy = .secondsSince1970
        }
    }
}

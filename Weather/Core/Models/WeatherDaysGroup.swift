//
//  WeatherDaysGroup.swift
//  Weather
//
//  Created by Valery Silin on 23/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation
import SwiftDate


struct WeatherDaysGroup {
    let date: Date
    let days: [WeatherDay]
}

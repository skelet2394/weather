//
//  CityIdentifier.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation


struct CityIdentifier: Codable {
    let title: String
    let isoCountry: String
}

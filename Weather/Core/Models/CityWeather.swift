//
//  CityWeather.swift
//  Weather
//
//  Created by Valery Silin on 23/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation


struct CityWeather: Codable {
    struct City: Codable {
        let name: String
        let country: String
    }
    let list: [WeatherDay]
    let city: City
}

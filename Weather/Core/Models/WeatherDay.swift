//
//  WeatherDayDTO.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//


//{
//    "dt": 1563840000,
//    "main": {
//        "temp": 9.76,
//        "temp_min": 6.12,
//        "temp_max": 9.76,
//        "pressure": 1018.04,
//        "sea_level": 1018.04,
//        "grnd_level": 884.71,
//        "humidity": 64,
//        "temp_kf": 3.65
//    },
//    "weather": [
//    {
//    "id": 800,
//    "main": "Clear",
//    "description": "clear sky",
//    "icon": "01d"
//    }
//    ],
//    "clouds": {
//        "all": 3
//    },
//    "wind": {
//        "speed": 0.94,
//        "deg": 142.018
//    },
//    "sys": {
//        "pod": "d"
//    },
//    "dt_txt": "2019-07-23 00:00:00"
//},


import Foundation


struct WeatherDay: Codable {
    struct Main: Codable {
        let temp: Double
        let tempMin: Double
        let tempMax: Double
        
        enum CodingKeys: String, CodingKey {
            case temp
            case tempMin = "temp_min"
            case tempMax = "temp_max"
        }
    }
    
    
    struct Weather: Codable {
        let condition: String
        let icon: String
        
        enum CodingKeys: String, CodingKey {
            case condition = "main"
            case icon
        }
    }
    
    
    let dateUtc: Date
    let temperature: Main
    let weather: [Weather]
    let timezone: Double?
    
    enum CodingKeys: String, CodingKey {
        case dateUtc = "dt"
        case temperature = "main"
        case weather
        case timezone
    }
    
    var date: Date {
        return dateUtc.addingTimeInterval(timezone ?? 0)
    }
}

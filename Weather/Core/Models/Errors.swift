//
//  Errors.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Rswift


typealias Strings = R.string


enum Errors: LocalizedError {
    case unknown
    case unauthorized
    
    enum Mapping: LocalizedError {
        case unableToGetDataFromString
        case unableToCastObject
    }
    
    enum InternetConnection: LocalizedError {
        case offline
        
        var errorDescription: String? {
            return Strings.errors.offline()
        }
    }
    
    var errorDescription: String? {
        return Strings.errors.error()
    }
}

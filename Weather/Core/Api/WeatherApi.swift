//
//  WeatherApi.swift
//  Weather
//
//  Created by Valery Silin on 23/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa


protocol WeatherApi {
    func getWeatherFor(cities: [CityIdentifier]) -> Single<[CityWeather]>
    func getCurrentWeatherFor(city: CityIdentifier) -> Single<WeatherDay>
}


class WeatherApiImpl: WeatherApi {

    let provider = MoyaProvider<WeatherRouter>()
    
    func getWeatherFor(cities: [CityIdentifier]) -> Single<[CityWeather]> {
        let requests = cities.map({ [unowned self] in
            self.provider.rx.request(.forecast(city: $0))
                .asObservable()
                .filterSuccessfulStatusAndRedirectCodes()
                .retry(3)
                .asObservable()
                .map(CityWeather.self, using: DefaultJSONDecoder(), failsOnEmptyData: false)
                .materialize()
        })
        
        return Observable
            .zip(requests)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .map({ $0.map({ $0.element }).compactMap({ $0 }) })
            .filter({ !$0.isEmpty })
            .asSingle()
//        TODO: handle errors
    }
    
    func getCurrentWeatherFor(city: CityIdentifier) -> Single<WeatherDay> {
        
        return provider.rx
            .request(.current(city: city))
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .filterSuccessfulStatusAndRedirectCodes()
            .map(WeatherDay.self, using: DefaultJSONDecoder(), failsOnEmptyData: false)
            .retry(3)
    }
}

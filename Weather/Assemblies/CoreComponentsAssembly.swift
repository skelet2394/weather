//
//  CoreComponentsAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject
import SwinjectAutoregistration
import Alamofire


struct CoreComponentsAssembly: Assembly {
    func assemble(container: Container) {
   
        
        container.autoregister(Api.self, initializer: ApiImpl.init)
        container.autoregister(WeatherApi.self, initializer: WeatherApiImpl.init)
  
    }
}

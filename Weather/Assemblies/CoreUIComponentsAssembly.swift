//
//  CoreUIComponentsAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject

struct CoreUIComponentsAssembly: Assembly {
    func assemble(container: Container) {
        container.register(UIWindow.self) { _ in
            let window = UIWindow(frame: UIScreen.main.bounds)
            
            window.layer.cornerRadius = 4
            window.layer.masksToBounds = true
            
            return window
        }
        
        container.register(UINavigationController.self) { _ in
            let nc = UINavigationController()
            
            nc.navigationBar.isTranslucent = false
            
            return nc
        }
        
        container.register(CoordinatorsFactory.self) { r in
            let container = r.resolve(Container.self)!
            
            return CoordinatorsFactoryImpl(resolver: container.synchronize())
        }
    }
}

//
//  AppAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//


import Swinject
import SwinjectAutoregistration


struct AppAssembly: Assembly {
    func assemble(container: Container) {
        
        container.register(AppCoordinator.self) { r in
            return r.resolve(AppCoordinatorImpl.self)!
        }
        
        container.register(AppCoordinatorImpl.self) { r in
            let launchScreen = R.storyboard.launchScreen().instantiateInitialViewController()!
            
            return AppCoordinatorImpl(window: r~>,
                                      navigationController: r~>,
                                      launchController: launchScreen,
                                      factory: r~>,
                                      services: r~>)
            }.inObjectScope(.container)
    }
}

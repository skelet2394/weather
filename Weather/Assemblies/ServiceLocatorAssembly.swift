//
//  ServiceLocatorAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject


enum ContainerName {
    fileprivate static let root = "root"
    static let main = "main"
}


struct ServiceLocatorAssembly: Assembly {
    func assemble(container: Container) {
        container.register(Container.self, name: ContainerName.root) { _ in
            return UIApplication.container
        }
        
        container.register(Container.self) { r in
            let parent = r.resolve(Container.self, name: ContainerName.root)!
            let container = Container(parent: parent)
            
            Assembler(container: container).apply(assemblies: [
                AppAssembly(),
                CoreComponentsAssembly(),
                CoreUIComponentsAssembly(),
                ServicesAssembly(),
                ])
            
            return container
            }.inObjectScope(.container)
        
        container.register(Container.self, name: ContainerName.main) { r in
            let parent = r.resolve(Container.self)!
            let container = Container(parent: parent)
            
            Assembler(container: container).apply(assemblies: [
                MainAssembly()
                ])
            
            return container
        }
    }
}

//
//  ServicesAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject


struct ServicesAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(Services.self, initializer: ServicesImpl.init)
        container.autoregister(WeatherService.self, initializer: WeatherServiceImpl.init)
        container.autoregister(ImageService.self, initializer: ImageServiceImpl.init)
    }
}

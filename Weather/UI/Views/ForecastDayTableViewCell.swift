//
//  ForecastDayTableViewCell.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa


struct ForecastDayTableViewCellModel: TableViewCellModel {
    let rowHeight: CGFloat = 60
    
    let bindings: ForecastDayTableViewCellBindings
    
    func configure(cell: ForecastDayTableViewCell) {
        cell.configureWith(bindings: bindings)
    }
}


protocol ForecastDayTableViewCellBindings {
    func weatherIcon() -> Driver<UIImage?>
    func date() -> Driver<String?>
    func time() -> Driver<String?>
    func hiTemp() -> Driver<String?>
    func loTemp() -> Driver<String?>
    
    func dayTime() -> Driver<DayTime>
}


class ForecastDayTableViewCell: CommonInitTableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var hiTempLabel: UILabel!
    @IBOutlet weak var loTempLabel: UILabel!
    
    var disposeBag = DisposeBag()
    
    override func commonInit() {
        backgroundColor = .clear
    }
    
    func configureWith(bindings: ForecastDayTableViewCellBindings) {
        disposeBag = DisposeBag()
        
        disposeBag.insert([
            bindings.weatherIcon().drive(weatherImageView.rx.image),
            bindings.date().drive(dateLabel.rx.text),
            bindings.time().drive(timeLabel.rx.text),
            bindings.hiTemp().drive(hiTempLabel.rx.text),
            bindings.loTemp().drive(loTempLabel.rx.text),
            bindings.dayTime().drive(rx.contrastAppearance)
        ])
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
}


extension ForecastDayTableViewCell: ContrastAppearanceAppliable {
    func applyStyleWith(dayTime: DayTime) {
        let textColor: UIColor
        
        switch dayTime {
        case .day:
            textColor = .black
        case .night:
            textColor = .white
        }
        [dateLabel,
         timeLabel,
         hiTempLabel,
         loTempLabel,
        ].forEach({
            $0?.textColor = textColor
        })
    }
}


extension Reactive where Base: ForecastDayTableViewCell {
    var contrastAppearance: Binder<DayTime> {
        return Binder(base, binding: { cell, dayTime in
            cell.applyStyleWith(dayTime: dayTime)
        })
    }
}

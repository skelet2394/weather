//
//  ForecastCellHeaderView.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxCocoa
import RxSwift


struct ForecastHeaderCellModel: TableViewCellModel {
    let rowHeight: CGFloat = 300
    
    let bindings: ForecastHeaderCellBindings
    
    func configure(cell: ForecastHeaderCell) {
        cell.configureWith(bindings: bindings)
    }
}


protocol ForecastHeaderCellBindings {
    func city() -> Driver<String?>
    func weatherCondition() -> Driver<String?>
    func currentTemperature() -> Driver<String?>
    func time() -> Driver<String?>
    func hiTemp() -> Driver<String?>
    func loTemp() -> Driver<String?>
    
    func dayTime() -> Driver<DayTime>
}

class ForecastHeaderCell: CommonInitTableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherConditionLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    
    @IBOutlet weak var weekdayLabel: UILabel!
    
    @IBOutlet weak var hiTempLabel: UILabel!
    @IBOutlet weak var loTempLabel: UILabel!
    
    var disposeBag = DisposeBag()
    
    override func commonInit() {
        backgroundColor = .clear
    }
    
    func configureWith(bindings: ForecastHeaderCellBindings) {
        disposeBag = DisposeBag()
        disposeBag.insert([
            bindings.city().drive(cityLabel.rx.text),
            bindings.weatherCondition().drive(weatherConditionLabel.rx.text),
            bindings.currentTemperature().drive(currentTemperatureLabel.rx.text),
            Driver.just(Strings.common.today()).drive(weekdayLabel.rx.text),
            bindings.hiTemp().drive(hiTempLabel.rx.text),
            bindings.loTemp().drive(loTempLabel.rx.text),
            bindings.dayTime().drive(rx.contrastAppearance)
        ])
    }
    
    func applyStyleWith(dayTime: DayTime) {
        let textColor: UIColor
        
        switch dayTime {
        case .day:
            textColor = .black
        case .night:
            textColor = .white
        }
        
        [
            cityLabel,
            weatherConditionLabel,
            currentTemperatureLabel,
            weekdayLabel,
            hiTempLabel,
            loTempLabel,
        ].forEach({
            $0?.textColor = textColor
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
}


extension Reactive where Base: ForecastHeaderCell {
    var contrastAppearance: Binder<DayTime> {
        return Binder(base, binding: { cell, dayTime in
            cell.applyStyleWith(dayTime: dayTime)
        })
    }
}

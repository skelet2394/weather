//
//  CityWeatherCell.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa


struct CityWeatherCellModel: CollectionViewCellModel {
    let bindings: CityWeatherCellBindings
    
    func configure(cell: CityWeatherCell) {
        cell.configureWith(bindings: bindings)
    }
}


protocol CityWeatherCellBindings {
    func headerCellBindings() -> Driver<ForecastHeaderCellBindings?>
    func forecastDayCellsBindings() -> Driver<[ForecastDayTableViewCellBindings]>
    func dayTime() -> Driver<DayTime>
}


class CityWeatherCell: CommonInitCollectionViewCell, ContrastAppearanceAppliable {
    @IBOutlet weak var forecastTableView: UITableView!
    
    var disposeBag = DisposeBag()
    
    override func commonInit() {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        forecastTableView.delegate = self
        forecastTableView.layer.cornerRadius = 20
        forecastTableView.backgroundColor = UIColor(white: 1, alpha: 0.1)
        forecastTableView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: -10, right: -10)
        
        forecastTableView.register(models: [
            ForecastHeaderCellModel.self,
            ForecastDayTableViewCellModel.self,
        ])
    }
    
    func configureWith(bindings: CityWeatherCellBindings) {
        disposeBag = DisposeBag()
        let headerCellDriver = bindings.headerCellBindings()
            .map({ bindings -> [TableViewCellModelAny] in
                guard let bindings = bindings else {
                    return []
                }
                return [ForecastHeaderCellModel(bindings: bindings)]
            })
        let daysCellsDriver =
            bindings.forecastDayCellsBindings()
                .map({ cellBindings -> [TableViewCellModelAny] in
                    cellBindings.map(ForecastDayTableViewCellModel.init)
                })
        disposeBag
            .insert([
                Driver.combineLatest(headerCellDriver, daysCellsDriver)
                    .map({ $0 + $1 })
                    .drive(forecastTableView.rx.cellModels),
                bindings.dayTime().drive(onNext: { [weak self] in
                    self?.applyStyleWith(dayTime: $0)
                })
            ])
    }
    
    func applyStyleWith(dayTime: DayTime) {
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            switch dayTime {
            case .day:
                self?.forecastTableView.backgroundColor = UIColor(white: 0, alpha: 0.1)
            case .night:
                self?.forecastTableView.backgroundColor = UIColor(white: 1, alpha: 0.1)
            }
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}


extension CityWeatherCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return UITableView.automaticDimension
        }
        return 60
    }
}

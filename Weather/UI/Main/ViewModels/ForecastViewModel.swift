//
//  ForecastViewModel.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa
import SwiftDate


typealias ForecastViewModelBindings = ForecastViewControllerBindings


protocol ForecastViewModel {
    func bindings() -> ForecastViewModelBindings
}


class ForecastViewModelImpl {
    let services: Services
    let factory: MainViewModelsFactory
    
    let forecastRelay = BehaviorRelay<[CityWeatherViewModel]>(value: [])
    var currentPageRelay = BehaviorRelay<Int>(value: 0)
    
    var disposeBag = DisposeBag()
    
    init(services: Services,
         factory: MainViewModelsFactory) {
        self.services = services
        self.factory = factory
        
        getForecast()
    }
    
    func getForecast() {
        disposeBag = DisposeBag()
        services.weather
            .getWeatherFor(cities: [CityIdentifier(title: "Moscow", isoCountry: "RU"),
                                    CityIdentifier(title: "Saint Petersburg", isoCountry: "RU"),
                                    CityIdentifier(title: "New York", isoCountry: "US"),
                                    CityIdentifier(title: "Tokyo", isoCountry: "JP")])
            .map({ [unowned self] in
                $0.map({
                    self.factory.cityWeather(cityWeather: $0)
                })
            })
            .asObservable()
            .catchErrorJustReturn([]) //TODO: handle error
            .bind(to: forecastRelay)
            .disposed(by: disposeBag)
    }
}


extension ForecastViewModelImpl: ForecastViewModel {
    func bindings() -> ForecastViewModelBindings {
        return self
    }
}


extension ForecastViewModelImpl: ForecastViewControllerBindings {
    func cities() -> Driver<[CityWeatherCellBindings]> {
        return forecastRelay
            .map({ $0.map({ $0.bindings() }) })
            .asDriver(onErrorJustReturn: [])
    }
    
    func totalPages() -> Driver<Int> {
        return forecastRelay.map({ $0.count }).asDriver(onErrorJustReturn: 0)
    }
    
    func currentPage() -> Driver<Int> {
        return currentPageRelay.asDriver()
    }
    
    func setCurrentPage() -> Binder<Int> {
        return Binder(self, binding: { viewModel, page in
            viewModel.currentPageRelay.accept(page)
        })
    }
    
    func dayTime() -> Driver<DayTime> {
        let cities = forecastRelay.flatMap({
            Observable.merge($0.map({ $0.dayTimeRelay.asObservable() }))
        }).distinctUntilChanged().map({ _ in })
        let currentPage = currentPageRelay.map({ _ in })
        
        return Observable
            .merge(cities, currentPage)
            .throttle(0.3, scheduler: ConcurrentDispatchQueueScheduler(qos: .utility))
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .map({ _ -> DayTime in
                guard !self.forecastRelay.value.isEmpty else {
                    return .day
                }
                return self.forecastRelay.value[self.currentPageRelay.value].dayTimeRelay.value
            }).asDriver(onErrorJustReturn: .day)
    }
}

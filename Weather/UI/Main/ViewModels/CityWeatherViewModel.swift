//
//  CityWeatherViewModel.swift
//  Weather
//
//  Created by Valery Silin on 23/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa


typealias CityWeatherViewModelBindings = CityWeatherCellBindings


protocol CityWeatherViewModel {
    func bindings() -> CityWeatherViewModelBindings
    var dayTimeRelay: BehaviorRelay<DayTime> { get }
}


class CityWeatherViewModelImpl {
    let services: Services
    let factory: MainViewModelsFactory
    
    let weatherDaysRelay = BehaviorRelay<[WeatherDayViewModel]>(value: [])
    let dayTimeRelay = BehaviorRelay<DayTime>(value: .day)
    let todayWeatherRelay = BehaviorRelay<WeatherDayViewModel?>(value: nil)
    
    var requestDisposeBag = DisposeBag()
    let disposeBag = DisposeBag()
    
    init(cityWeather: CityWeather,
         factory: MainViewModelsFactory,
         services: Services) {
        self.services = services
        self.factory = factory
        
        setupUpdateDateTime()

        getCurrentWeather(city: CityIdentifier(title: cityWeather.city.name,
                                               isoCountry: cityWeather.city.country))
        
        let days = cityWeather.list
            .map({ weatherDay -> WeatherDayViewModel in
                factory.weatherDay(weatherDay: weatherDay,
                                                   city: cityWeather.city.name)
            })
        self.weatherDaysRelay.accept(days)
        
    }
    
    func setupUpdateDateTime() {
        dayTimeRelay
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .bind(onNext: { [unowned self] dayTime in
                self.weatherDaysRelay.value
                    .forEach({
                        $0.set(dayTime: dayTime)
                    })
            }).disposed(by: disposeBag)
    }
    
    func getCurrentWeather(city: CityIdentifier) {
        requestDisposeBag = DisposeBag()
        services.weather
            .getCurrentWeatherFor(city: city)
            .map({ [unowned self] weatherDay -> WeatherDayViewModel in
                let viewModel = self.factory.weatherDay(weatherDay: weatherDay,
                                                   city: city.title)
                let dayTime = self.dayOrNight(date: weatherDay.date)
                viewModel.set(dayTime: dayTime)
                self.dayTimeRelay.accept(dayTime)
                
                return viewModel
            }).asObservable()
            .bind(to: todayWeatherRelay)
        .disposed(by: requestDisposeBag)
    }
    
    func dayOrNight(date: Date) -> DayTime {
        guard date.hour > 18 || date.hour < 6 else {
            return .day
        }
        return .night
    }
}


extension CityWeatherViewModelImpl: CityWeatherViewModel {
    func bindings() -> CityWeatherViewModelBindings {
        return self
    }
}

extension CityWeatherViewModelImpl: CityWeatherCellBindings {
    func dayTime() -> Driver<DayTime> {
        return dayTimeRelay.asDriver()
    }
    
    func headerCellBindings() -> Driver<ForecastHeaderCellBindings?> {
        return todayWeatherRelay
            .map({ $0?.bindings() })
            .asDriver(onErrorJustReturn: nil)
    }
    
    func forecastDayCellsBindings() -> Driver<[ForecastDayTableViewCellBindings]> {
        return weatherDaysRelay
            .map({ $0.map({ $0.bindings() }) })
            .asDriver(onErrorJustReturn: [])
    }
}

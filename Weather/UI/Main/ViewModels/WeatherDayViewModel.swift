//
//  WeatherDayViewModel.swift
//  Weather
//
//  Created by Valery Silin on 23/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa
import SwiftDate

typealias WeatherDayViewModelBindings = ForecastDayTableViewCellBindings & ForecastHeaderCellBindings


protocol WeatherDayViewModel {
    func bindings() -> WeatherDayViewModelBindings
    func set(dayTime: DayTime)
}


class WeatherDayViewModelImpl {
    let services: Services
    let cityString: String
    
    let weatherRelay: BehaviorRelay<WeatherDay>
    
    let dayTimeRelay = BehaviorRelay<DayTime>(value: .day)
    
    init(city: String,
         weatherDay: WeatherDay,
         services: Services) {
        self.cityString = city
        self.services = services
        self.weatherRelay = BehaviorRelay<WeatherDay>(value: weatherDay)
    }
}


extension WeatherDayViewModelImpl: WeatherDayViewModel {
    func bindings() -> WeatherDayViewModelBindings {
        return self
    }
    func set(dayTime: DayTime) {
        dayTimeRelay.accept(dayTime)
    }
}


extension WeatherDayViewModelImpl: ForecastDayTableViewCellBindings {
    func weatherIcon() -> Driver<UIImage?> {
        return services.image
            .imageWith(observable: weatherRelay.map({
                guard let imageName = $0.weather.first?.icon else {
                    return nil
                }
                return imageName + "@2x.png"
            }))
            .asDriver()
    }
    
    func hiTemp() -> Driver<String?> {
        return weatherRelay
            .map({ String(Int($0.temperature.tempMax)) })
            .asDriver(onErrorJustReturn: nil)
    }
    
    func loTemp() -> Driver<String?> {
        return weatherRelay
            .map({ String(Int($0.temperature.tempMin)) })
            .asDriver(onErrorJustReturn: nil)
    }
    
    func dayTime() -> Driver<DayTime> {
        return dayTimeRelay.asDriver()
    }
}


extension WeatherDayViewModelImpl: ForecastHeaderCellBindings {
    func weekday() -> Driver<String?> {
        return weatherRelay
            .map({ $0.date.toFormat("EEEE") })
            .asDriver(onErrorJustReturn: nil)
    }
    
    func city() -> Driver<String?> {
        return .just(cityString)
    }
    
    func weatherCondition() -> Driver<String?> {
        return weatherRelay
            .map({ $0.weather.first?.condition })
            .asDriver(onErrorJustReturn: nil)
    }
    
    func currentTemperature() -> Driver<String?> {
        return weatherRelay
            .map({ String(Int($0.temperature.temp)) })
            .asDriver(onErrorJustReturn: nil)
    }
    
    func date() -> Driver<String?> {
        return weatherRelay
            .map({ $0.date.weekdayName(.default) })
            .asDriver(onErrorJustReturn: nil)
    }
    
    func time() -> Driver<String?> {
        return weatherRelay
            .map({ $0.date.toFormat("HH:mm") })
            .asDriver(onErrorJustReturn: nil)
        
    }
}

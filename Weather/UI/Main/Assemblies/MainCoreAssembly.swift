//
//  MainCoreAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject
import SwinjectAutoregistration


struct MainCoreAssembly: Assembly {
    func assemble(container: Container) {
        container.register(MainCoordinator.self) { (r, navigationController: UINavigationController, onComplete: MainCoordinatorImpl.CompletionBlock?) in
            return MainCoordinatorImpl(factory: r~>,
                                       navigationController: navigationController,
                                       onComplete: onComplete)
        }
        
        container.register(MainViewControllersFactory.self) { r in
            let container = r.resolve(Container.self, name: ContainerName.main)!
            
            return MainViewControllersFactoryImpl(resolver: container.synchronize())
        }
        
        container.register(MainViewModelsFactory.self) { r in
            let container = r.resolve(Container.self, name: ContainerName.main)!
            
            return MainViewModelsFactoryImpl(resolver: container.synchronize())
        }
    }
}


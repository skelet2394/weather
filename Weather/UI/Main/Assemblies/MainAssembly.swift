//
//  MainAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject


struct MainAssembly: Assembly {
    let assemblies: [Assembly] = [
        MainCoreAssembly(),
        MainForecastAssembly(),
    ]
    
    func assemble(container: Container) {
        assemblies.forEach({ $0.assemble(container: container) })
    }
}

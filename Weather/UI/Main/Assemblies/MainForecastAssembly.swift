//
//  MainForecastAssembly.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject


struct MainForecastAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(ForecastViewModel.self, initializer: ForecastViewModelImpl.init)
        
        container.autoregister(CityWeatherViewModel.self,
                               argument: CityWeather.self,
                               initializer: CityWeatherViewModelImpl.init)
            .inObjectScope(.transient)
        
        container.autoregister(WeatherDayViewModel.self,
                               arguments: WeatherDay.self, String.self,
                               initializer: WeatherDayViewModelImpl.init)
        .inObjectScope(.transient)
        
        container.register(ForecastViewController.self, factory: { r in
            let vc = ForecastViewController()
            let viewModel = r.resolve(ForecastViewModel.self)!
            
            vc.bindings = viewModel.bindings()
            
            return vc
        })
        
    }
}

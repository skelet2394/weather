//
//  MainCoordinator.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit


protocol MainCoordinator: BaseCoordinator { }


struct MainCoordinatorImpl: MainCoordinator, FlowController {
    enum Event {
        
    }
    
    let factory: MainViewControllersFactory
    let navigationController: UINavigationController

    var onComplete: ((MainCoordinatorImpl.Event) -> ())?
    
    func start() {
        runForecast()
    }
}


extension MainCoordinatorImpl {
    func runForecast() {
        let vc = factory.forecast()
        
        vc.onComplete = {
            switch $0 {
            case .chooseCity:
                break
                //TODO:
            }
        }
        navigationController.pushViewController(vc, animated: true)
        navigationController.setNavigationBarHidden(true, animated: true)
    }
}

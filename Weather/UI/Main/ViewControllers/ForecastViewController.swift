//
//  DetailedWeatherViewController.swift
//  Weather
//
//  Created by Valery Silin on 20/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa


protocol ForecastViewControllerBindings {
    func cities() -> Driver<[CityWeatherCellBindings]>
    func currentPage() -> Driver<Int>
    func setCurrentPage() -> Binder<Int>
    func totalPages() -> Driver<Int>
    func dayTime() -> Driver<DayTime>
}


class ForecastViewController: ViewController, FlowController, ContrastAppearanceAppliable {
    enum Event {
        case chooseCity
    }
    var onComplete: ((ForecastViewController.Event) -> ())?
    
    var bindings: ForecastViewControllerBindings!
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var cardsCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardsCollection.allowsSelection = false
        cardsCollection.delegate = self
        cardsCollection.register(model: CityWeatherCellModel.self)
        cardsCollection.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func startSubscription() {
        disposeBag.insert([
            bindings.cities()
                .map({ $0.map(CityWeatherCellModel.init) })
                .drive(cardsCollection.rx.cellModels),
            bindings.totalPages()
                .drive(pageControl.rx.numberOfPages),
            bindings.currentPage()
                .drive(pageControl.rx.currentPage),
            bindings.dayTime()
                .distinctUntilChanged()
                .drive(onNext: { [weak self] in
                    self?.applyStyleWith(dayTime: $0)
            })
            
        ])
    }
    
    func applyStyleWith(dayTime: DayTime) {
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            switch dayTime {
            case .day:
                self?.view.backgroundColor = UIApplication.configuration.dayColor
                self?.pageControl.pageIndicatorTintColor = UIColor(white: 0, alpha: 0.3)
                self?.pageControl.currentPageIndicatorTintColor = .black
            case .night:
                self?.view.backgroundColor = UIApplication.configuration.nightColor
                self?.pageControl.pageIndicatorTintColor = UIColor(white: 1, alpha: 0.3)
                self?.pageControl.currentPageIndicatorTintColor = .white
            }
        })
    }
}


extension ForecastViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.width + 0.5)
        bindings?.setCurrentPage().onNext(page)
    }
}

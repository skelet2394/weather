//
//  MainViewModelsFactory.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject


protocol MainViewModelsFactory {
    func cityWeather(cityWeather: CityWeather) -> CityWeatherViewModel
    func weatherDay(weatherDay: WeatherDay, city: String) -> WeatherDayViewModel
}


struct MainViewModelsFactoryImpl: MainViewModelsFactory {
    let resolver: Resolver
    
    func cityWeather(cityWeather: CityWeather) -> CityWeatherViewModel {
        return resolver.resolve(CityWeatherViewModel.self, argument: cityWeather)!
    }
    
    func weatherDay(weatherDay: WeatherDay, city: String) -> WeatherDayViewModel {
        return resolver.resolve(WeatherDayViewModel.self, arguments: weatherDay, city)!
    }

}

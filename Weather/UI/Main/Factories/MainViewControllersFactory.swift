//
//  MainViewControllersFactory.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject
import SwinjectAutoregistration


protocol MainViewControllersFactory {
    func navigation(root: UIViewController?) -> UINavigationController

    func forecast() -> ForecastViewController
}


struct MainViewControllersFactoryImpl: MainViewControllersFactory {
    
    let resolver: Resolver
    
    func navigation(root: UIViewController?) -> UINavigationController {
        let nc = resolver.resolve(UINavigationController.self)!
        
        if let root = root {
            nc.setViewControllers([root], animated: false)
        }
        
        return nc
    }
    
    
    func forecast() -> ForecastViewController {
        return resolver.resolve(ForecastViewController.self)!
    }
    
}

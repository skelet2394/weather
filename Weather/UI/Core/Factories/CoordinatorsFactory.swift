//
//  CoordinatorsFactory.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject


protocol CoordinatorsFactory {
    func main(navigationController: UINavigationController,
              onComplete: MainCoordinatorImpl.CompletionBlock?) -> MainCoordinator
}


struct CoordinatorsFactoryImpl: CoordinatorsFactory {
    let resolver: Resolver
    
    func main(navigationController: UINavigationController,
              onComplete: MainCoordinatorImpl.CompletionBlock?) -> MainCoordinator {
        let container = resolver.resolve(Container.self, name: ContainerName.main)!
        
        return container.synchronize().resolve(MainCoordinator.self, arguments: navigationController, onComplete)!
    }
   
}

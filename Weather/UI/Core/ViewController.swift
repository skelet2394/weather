//
//  ViewController.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa


class ViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        startSubscription()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopSubscription()
    }
    
    /// Метод вызывается при viewWillAppear для подписки
    func startSubscription() {
        
    }
    
    /// Метод вызывается при viewDidDisappear для подписки
    func stopSubscription() {
        
    }
}

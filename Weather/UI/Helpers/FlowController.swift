//
//  FlowController.swift
//  Weather
//
//  Created by Valery Silin on 20/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation


protocol FlowController {
    associatedtype Event
    
    typealias CompletionBlock = (Event) -> ()
    
    var onComplete: CompletionBlock? { get set }
}

extension FlowController {
    func complete(_ event: Event) {
        if Thread.isMainThread {
            self.onComplete?(event)
            return
        }
        
        DispatchQueue.main.async { [self] in
            self.onComplete?(event)
        }
    }
}

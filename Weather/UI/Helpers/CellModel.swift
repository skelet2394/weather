//
//  CellModel.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa


protocol CellModelAny {
    static var cellTypeAny: UIView.Type { get }
    static var reuseId: String { get }
    
    func configureAny(cell: UIView)
}


extension CellModelAny {
    static var reuseId: String {
        return String(describing: Self.cellTypeAny.self)
    }
}


protocol CollectionViewCellModel: CellModelAny {
    associatedtype CellType: UICollectionViewCell
    func configure(cell: CellType)
}


extension CollectionViewCellModel {
    static var cellTypeAny: UIView.Type {
        return CellType.self
    }
    
    func configureAny(cell: UIView) {
        guard let cell = cell as? CellType else {
            return
        }
        
        configure(cell: cell)
    }
    
    func asAny() -> CellModelAny {
        return self
    }
}


protocol TableViewCellModelAny: CellModelAny {
    var rowHeight: CGFloat { get }
}


protocol TableViewCellModel: TableViewCellModelAny {
    associatedtype CellType: UITableViewCell
    func configure(cell: CellType)
}


extension TableViewCellModel {
    static var cellTypeAny: UIView.Type {
        return CellType.self
    }
    
    func configureAny(cell: UIView) {
        guard let cell = cell as? CellType else {
            return
        }
        
        configure(cell: cell)
    }
    
    func asAny() -> CellModelAny {
        return self
    }
}


extension UITableView {
    func register(model: CellModelAny.Type) {
        guard Bundle.main.path(forResource: model.reuseId, ofType: "nib") != nil else {
            register(model.cellTypeAny.self, forCellReuseIdentifier: model.reuseId)
            return
        }
        
        let nib = UINib(nibName: model.reuseId, bundle: nil)
        register(nib, forCellReuseIdentifier: model.reuseId)
    }
    
    func register(models: [CellModelAny.Type]) {
        models.forEach { register(model: $0) }
    }
    
    func dequeueReusableCell(withModel model: CellModelAny, indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: type(of: model).reuseId, for: indexPath)
        
        model.configureAny(cell: cell)
        
        return cell
    }
}


extension UICollectionView {
    func register(model: CellModelAny.Type) {
        let nib = UINib(nibName: model.reuseId, bundle: nil)
        register(nib, forCellWithReuseIdentifier: model.reuseId)
    }
    
    func register(models: [CellModelAny.Type]) {
        models.forEach { register(model: $0) }
    }
    
    func dequeueReusableCell(withModel model: CellModelAny, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: type(of: model).reuseId, for: indexPath)
        
        model.configureAny(cell: cell)
        
        return cell
    }
}


extension Reactive where Base: UITableView {
    func cellModels<S: Sequence, O: ObservableType>(_ source: O)
        -> Disposable where O.E == S, S.Element: CellModelAny {
            return self.items(source)({ tableView, index, cellModel in
                let indexPath = IndexPath(row: index, section: 0)
                return tableView.dequeueReusableCell(withModel: cellModel, indexPath: indexPath)
            })
    }
    
    func cellModels<S: Sequence, O: ObservableType>(_ source: O)
        -> Disposable where O.E == S, S.Element == CellModelAny {
            return self.items(source)({ tableView, index, cellModel in
                let indexPath = IndexPath(row: index, section: 0)
                return tableView.dequeueReusableCell(withModel: cellModel, indexPath: indexPath)
            })
    }
    
    func cellModels<S: Sequence, O: ObservableType>(_ source: O)
        -> Disposable where O.E == S, S.Element == TableViewCellModelAny {
            return self.items(source)({ tableView, index, cellModel in
                let indexPath = IndexPath(row: index, section: 0)
                return tableView.dequeueReusableCell(withModel: cellModel, indexPath: indexPath)
            })
    }
}


extension Reactive where Base: UICollectionView {
    func cellModels<S: Sequence, O: ObservableType>(_ source: O)
        -> Disposable where O.E == S, S.Element: CellModelAny {
            return self.items(source)({ collectionView, index, cellModel in
                let indexPath = IndexPath(item: index, section: 0)
                return collectionView.dequeueReusableCell(withModel: cellModel, indexPath: indexPath)
            })
    }
    
    func cellModels<S: Sequence, O: ObservableType>(_ source: O)
        -> Disposable where O.E == S, S.Element == CellModelAny {
            return self.items(source)({ collectionView, index, cellModel in
                let indexPath = IndexPath(item: index, section: 0)
                return collectionView.dequeueReusableCell(withModel: cellModel, indexPath: indexPath)
            })
    }
}

//
//  BaseCoordinator.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation


protocol BaseCoordinator {
    func start()
}

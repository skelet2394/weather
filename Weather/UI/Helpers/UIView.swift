//
//  UIView.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit


extension UIView {
    func loadFromNib(named nibName: String? = nil) {
        let nibName = nibName ?? String(describing: type(of: self))
        
        let contents = Bundle.main.loadNibNamed(nibName, owner: self, options: [:])
        guard let contentView = contents?.first as? UIView else {
            fatalError("Первый объект в ксибе не найден или не является UIView")
        }
        
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}

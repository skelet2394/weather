//
//  ContrastAppearance.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa


protocol ContrastAppearanceAppliable {
    func applyStyleWith(dayTime: DayTime)
}

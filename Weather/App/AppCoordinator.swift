//
//  AppCoordinator.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit


protocol AppCoordinator: BaseCoordinator { }


struct AppCoordinatorImpl {
    let window: UIWindow
    let navigationController: UINavigationController
    let launchController: UIViewController
    let factory: CoordinatorsFactory
    let services: Services
}


extension AppCoordinatorImpl: AppCoordinator {
    func start() {
        window.makeKeyAndVisible()
        window.rootViewController = navigationController
        runMain()
    }
}


extension AppCoordinatorImpl {
    func runMain() {
        let coordinator = factory.main(navigationController: navigationController, onComplete: { _ in })
        
        coordinator.start()
    }
}

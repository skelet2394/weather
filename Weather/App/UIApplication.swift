//
//  UIApplication.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Swinject


let Config = UIApplication.configuration


extension UIApplication {
    static let container: Container = {
        let container = Container()
        
        Assembler(container: container).apply(assemblies: [
            ServiceLocatorAssembly()
            ])
        
        return container
    }()
    
    static let configuration: AppConfiguration = {
       return AppConfigurationImpl()
    }()
    
}

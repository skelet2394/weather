//
//  AppDelegate.swift
//  Weather
//
//  Created by Valery Silin on 20/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit
import Swinject


@UIApplicationMain
class AppDelegate: UIResponder {
    
    var resolver: Resolver {
        return UIApplication.container.resolve(Container.self)!.synchronize()
    }
    
    var configuration: AppConfiguration {
        return resolver.resolve(AppConfiguration.self)!
    }
    
    var appCoordinator: AppCoordinator {
        return resolver.resolve(AppCoordinator.self)!
    }
}


extension AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        appCoordinator.start()
        return true
    }
}


//
//  AppConfiguration.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit


protocol AppConfiguration {
    var apiToken: String { get }
    var baseUrl: String { get }
    var imageUrl: String { get }
    var nightColor : UIColor { get }
    var dayColor : UIColor { get }
}


struct AppConfigurationImpl: AppConfiguration {
    let apiToken: String = "5ccea88a39f70bbfd3501b07b95933c3"
    let baseUrl: String = "https://api.openweathermap.org/data/2.5"
    let imageUrl: String = "https://openweathermap.org/img/wn/"
    
    let nightColor = UIColor(red: 0.14, green: 0.25, blue: 0.38, alpha: 1)
    let dayColor = UIColor(red: 0.96, green: 0.98, blue: 0.91, alpha: 1)
}

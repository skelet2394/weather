//
//  WeatherRouter.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Moya


enum WeatherRouter {
    case forecast(city: CityIdentifier)
    case current(city: CityIdentifier)
}


extension WeatherRouter: TargetType {
    var baseURL: URL {
        return URL(string: UIApplication.configuration.baseUrl)!
    }
    
    var path: String {
        switch self {
        case .forecast:
            return "/forecast"
        case .current:
            return "/weather"
        }
    }
    
    var method: Method {
        switch self {
        case .forecast, .current:
            return .get
        }
    }
    
    var sampleData: Data {
        return "".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        case .forecast(let city), .current(let city):
            let parameters: [String: Any] = ["q": "\(city.title),\(city.isoCountry)",
                                             "units": "metric",
                                             "appid": UIApplication.configuration.apiToken]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type" : "application/json"]
    }
}

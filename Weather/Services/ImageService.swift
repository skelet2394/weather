//
//  ImageService.swift
//  Weather
//
//  Created by Valery Silin on 24/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa
import RxNuke
import Nuke


protocol ImageService {
    func loadImage(url: String?) -> Single<UIImage?>
    func imageWith(observable: Observable<String?>) -> Driver<UIImage?>
}


struct ImageServiceImpl: ImageService {
    let imagesUrl = UIApplication.configuration.imageUrl
    
    func loadImage(url: String?) -> Single<UIImage?> {
        guard var urlString = url else {
            return .just(nil)
        }
        
        if !urlString.hasPrefix("http") {
            urlString = imagesUrl + urlString
        }
        
        guard let url = URL(string: urlString) else {
            return .just(nil)
        }
        
        let request = ImageRequest(url: url)
        return ImagePipeline.shared.rx.loadImage(with: request).map({ $0.image })
    }
    
    func imageWith(observable: Observable<String?>) -> Driver<UIImage?> {
        return observable.flatMapLatest({
            return self.loadImage(url: $0)
        }).asDriver(onErrorJustReturn: nil)
    }
}

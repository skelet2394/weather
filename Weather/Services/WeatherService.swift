//
//  WeatherService.swift
//  Weather
//
//  Created by Valery Silin on 22/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya


protocol WeatherService {
    func getWeatherFor(cities: [CityIdentifier]) -> Single<[CityWeather]>
    func getCurrentWeatherFor(city: CityIdentifier) -> Single<WeatherDay>
}


struct WeatherServiceImpl: WeatherService {
    
    let api: Api
    
    func getWeatherFor(cities: [CityIdentifier]) -> Single<[CityWeather]> {
        return api.weather.getWeatherFor(cities: cities)
    }
    
    func getCurrentWeatherFor(city: CityIdentifier) -> Single<WeatherDay> {
        return api.weather.getCurrentWeatherFor(city: city)
    }
}

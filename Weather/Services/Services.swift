//
//  Services.swift
//  Weather
//
//  Created by Valery Silin on 21/07/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation


protocol Services {
    var weather: WeatherService { get }
    var image: ImageService { get }
}


struct ServicesImpl: Services {
    let weather: WeatherService
    let image: ImageService
}
